// 主体内容
import './main.css'
import '../title'
import {getJson} from 'api/Ajax'
import render from './main.art'
import {DETAIL} from 'api/config'

const main = document.getElementById('main');

getJson(DETAIL+location.search.split('=')[1])
.then(data => {
    main.innerHTML = render(data);
})
.catch(error => {
    console.log(error);
})
