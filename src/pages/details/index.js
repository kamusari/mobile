/* 详情页入口 */

// 公共样式
import 'styles/reset.css'
import 'styles/base.css'
import 'styles/loading.css'
// 详情页样式
import './details.css'
// 公共组件
import 'components/topbar'
// 详情页组件
import './components/header'
import './components/main'
import './components/tapbar'