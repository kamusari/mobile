/* 首页入口 */

// 公共样式
import 'styles/reset.css'
import 'styles/base.css'
import 'styles/loading.css'
// 首页样式
import './index.css'
// 公共组件
import 'components/tabbar'
import 'components/topbar'
import BackTop from 'components/backTop';
import 'components/search'
// 首页组件
import './components/header'
import './components/slider'
import './components/nav'
import './components/product'

// 调用返回顶部按钮类
new BackTop(document.getElementById('main'));

