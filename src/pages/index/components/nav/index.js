/* 导航 */

import './nav.css'
import {getJson} from 'api/Ajax'
import render from './nav.art'
import {NAV} from 'api/config'

const nav = document.getElementById('nav');

getJson(NAV)
.then(data => {
    nav.innerHTML = render({data});
})
.catch(error => {
    console.log(error);
})