/* 轮播图组件 */
import './slider.css'
import 'styles/swiper-bundle.min.css'
import Swiper from 'utils/swiper-bundle.min'
import {getJson} from 'api/Ajax'
import {SLIDER} from 'api/config'
import render from './slider.art'

const slider = document.getElementById('slider');

getJson(SLIDER)
.then(data => {
    slider.innerHTML = render({data});
    new Swiper('.swiper', {
        loop: true, // 循环模式选项
    
        // 如果需要分页器
        pagination: {
            el: '.swiper-pagination',
        },
        
        // 自动播放
        autoplay: {
            delay: 3000,
            disableOnInteraction: false
        },
    });
})
.catch(error => {
    console.log(error);
})




