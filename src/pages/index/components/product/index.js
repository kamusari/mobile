/* 精选折扣 */
import './product.css'
import {getJson} from 'api/Ajax'
import render from './product.art'
import {PRODUCT} from 'api/config'
import {CODE} from 'api/config'

const product = document.getElementById('product-box');

getJson(PRODUCT+CODE)
.then(data => {
    product.innerHTML = render({data});
})
.catch(error => {
    console.log(error);
})