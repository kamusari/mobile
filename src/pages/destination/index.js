/* 目的地入口 */

// 公共样式
import 'styles/reset.css'
import 'styles/base.css'
// 目的地样式
import './destination.css'
// 公共组件
import 'components/tabbar'
// 目的地组件
import './components/header'
import './components/main'

