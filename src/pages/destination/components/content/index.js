// 内容组件
import './content.css'

class Content {
    constructor(el){
        this.el = el;
    }
    // 改变页面内容
    setText(content){
        this.el.innerHTML = content;
    }
}

export default Content