// 侧边栏组件
import './sidebar.css'
import {getJson} from 'api/Ajax'
import {DESTINATION} from 'api/config'
import {SIDEBAR_ITEM_ACTIVE_CLASSNAME} from './constants' 

// 侧边栏类
class SideBar {
    constructor(el){
        this.el = el;
        this.lis = el.children;
        this.request = null;
    }

    // 改变标签样式
    change(li){
        for(const item of this.lis){
            item.classList.remove(SIDEBAR_ITEM_ACTIVE_CLASSNAME);
        }
        li.classList.add(SIDEBAR_ITEM_ACTIVE_CLASSNAME);
    }
    // 获取数据
    getData(index){

        if(this.request) this.request.xhr.abort();

        this.request = getJson(DESTINATION+index);

        return this.request;
    }

}

export default SideBar;