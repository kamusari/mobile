/* 个人主页入口 */

// 公共样式
import 'styles/reset.css'
import 'styles/base.css'
// 个人主页样式
import './personal.css'
// 公共组件
import 'components/tabbar'
// 个人主页组件
import './components/header'
import './components/main'