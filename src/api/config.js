
/* 验证码6-18生成，有效期30天 */
export const CODE = 'JB1D41FA03075019D';

/* 轮播图 */
export const SLIDER = 'https://www.imooc.com/api/mall-wepApp/index/slider';

/* 导航 */
export const NAV = 'https://www.imooc.com/api/mall-wepApp/index/nav';

/* 精选折扣 */
export const PRODUCT = 'https://www.imooc.com/api/mall-wepApp/index/product?icode=';

/* 详情页 */
export const DETAIL = 'https://www.imooc.com/api/mall-wepApp/details/';

/* 目的地 */
export const DESTINATION = 'https://www.imooc.com/api/mall-wepApp/destination/content/';