/* 返回顶部 */
import 'styles/iconfont.css'
import './backTop.css'
import { BACKTOP_ACTIVE_CLASSNAME } from './constants';

// 添加防抖效果
import debounce from 'utils/debounce'

const btn = document.getElementById('backTop');

class BackTop {
    constructor(el) {
        this.el = el;
        this.bindEvent();
    }
    bindEvent() {
        // 返回按钮事件监听
        btn.addEventListener('click', () => {
            this.el.scrollTo({
                top: 0,
                behavior: 'smooth'
            })
        });
        // 滚动监听，防抖处理
        this.el.addEventListener('scroll', debounce(this.show, this.el),100);
    }

    // 显示返回顶部按钮
    show() {
        if (this.scrollTop >= this.offsetHeight / 2) {
            btn.classList.add(BACKTOP_ACTIVE_CLASSNAME);
        } else {
            btn.classList.remove(BACKTOP_ACTIVE_CLASSNAME);
        }
    }
}

export default BackTop;




