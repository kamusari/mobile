/* 顶部标签 */
import './topbar.css'
import { TOPBAR_ACTIVE_CLASSNAME } from './constants';

const topBar = document.getElementById('topbar');
const main = document.getElementById('main');

main.addEventListener('scroll',()=>{

    if(show()){
        topBar.classList.add(TOPBAR_ACTIVE_CLASSNAME);
    }
    if(hide()){
        topBar.classList.remove(TOPBAR_ACTIVE_CLASSNAME);
    }
})

// 是否显示
function show(){
    return main.scrollTop > 20 && !topBar.classList.contains(TOPBAR_ACTIVE_CLASSNAME);
}

// 是否隐藏
function hide(){
    return main.scrollTop < 20 && topBar.classList.contains(TOPBAR_ACTIVE_CLASSNAME);
}