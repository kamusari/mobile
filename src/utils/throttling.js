/* 节流 */
export default (fn,that,time=200) => {
    let lock = false;
    return function(...args){
        if(lock) return;
        fn.apply(that||this,args);
        lock = true;
        setTimeout(()=>lock = false,time);
    }
}