/* 防抖 */
export default (fn,that,time=200) => {
    let timer = null;
    return function(...args){
        if(timer) clearTimeout(timer);
        timer = setTimeout(()=>{
            fn.apply(that||this,args);
        },time)
    }
}