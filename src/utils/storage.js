/* 会话存储功能封装 */
// 获取
export function get(key){
    return JSON.parse(sessionStorage.getItem(key));
}
// 添加
export function set(key,value){
    sessionStorage.setItem(key,JSON.stringify(value));
}
// 删除
export function remove(key){
    sessionStorage.removeItem(key);
}
// 清空
export function clear(){
    sessionStorage.clear();
}
