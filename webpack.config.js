const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const resolve = data => path.resolve(__dirname, data);

module.exports = {

    mode: 'development',

    devtool: 'inline-source-map',

    resolve: {
        // 路径别名
        alias: {
            api: resolve('src/api'),
            utils: resolve('src/utils'),
            components: resolve('src/components'),
            pages: resolve('src/pages'),
            fonts: resolve('src/assets/fonts'),
            images: resolve('src/assets/images'),
            styles: resolve('src/assets/styles'),
        },
    },

    entry: {
        index:'./src/pages/index',
        destination:'./src/pages/destination',
        details:'./src/pages/details',
        personal:'./src/pages/personal'
    },

    output: {
        path: resolve('dist'),
        filename: 'js/[name].js',
        // 静态资源模块文件名
        assetModuleFilename: 'assets/[name][ext][query]',
        // 打包的时候打开，清除dist目录，开发环境下注释掉，会报错
        clean:true
    },

    module: {
        rules: [
            {
                test: /\.art$/i,
                loader: 'art-template-loader'
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset',
                // 控制图片是否转化为base64编码格式
                parser: {
                    dataUrlCondition: {
                        maxSize: 4 * 1024 // 4kb,默认值是8kb
                    }
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset',
                parser: {
                    dataUrlCondition: {
                        maxSize: 4 * 1024 // 4kb,默认值是8kb
                    }
                }
            },
        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: 'style/[name].css'
        }),
        // 首页
        new HtmlWebpackPlugin({
            template: './src/pages/index/index.art',
            filename: 'index.html',
            chunks:['index'],
            minify: {
                // 删除 index.html 中的注释
                removeComments: true,
                // 删除 index.html 中的空格
                collapseWhitespace: false,
                // 删除各种 html 标签属性值的双引号
                removeAttributeQuotes: true
            }
        }),
        // 目的地
        new HtmlWebpackPlugin({
            template: './src/pages/destination/destination.art',
            filename: 'destination.html',
            chunks:['destination'],
            minify: {
                // 删除 index.html 中的注释
                removeComments: true,
                // 删除 index.html 中的空格
                collapseWhitespace: false,
                // 删除各种 html 标签属性值的双引号
                removeAttributeQuotes: true
            }
        }),
        // 详情
        new HtmlWebpackPlugin({
            template: './src/pages/details/details.art',
            filename: 'details.html',
            chunks:['details'],
            minify: {
                // 删除 index.html 中的注释
                removeComments: true,
                // 删除 index.html 中的空格
                collapseWhitespace: false,
                // 删除各种 html 标签属性值的双引号
                removeAttributeQuotes: true
            }
        }),
        // 个人
        new HtmlWebpackPlugin({
            template: './src/pages/personal/personal.art',
            filename: 'personal.html',
            chunks:['personal'],
            minify: {
                // 删除 index.html 中的注释
                removeComments: true,
                // 删除 index.html 中的空格
                collapseWhitespace: false,
                // 删除各种 html 标签属性值的双引号
                removeAttributeQuotes: true
            }
        })

    ]


}